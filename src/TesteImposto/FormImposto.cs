﻿using System;
using System.Data;
using System.Windows.Forms;
using Imposto.Domain.Models;
using Imposto.DataAccess.Services;

namespace TesteImposto
{
    public partial class FormImposto : Form
    {
        private readonly INotaFiscalService _notaFiscalService;
        public FormImposto()
        {
            //TODO: DI
            _notaFiscalService = new NotaFiscalService();

            InitializeComponent();

            dataGridViewPedidos.AutoGenerateColumns = true;
            dataGridViewPedidos.DataSource = GetTablePedidos();
            ResizeColumns();

            PopularComboEstadosOrigem();
            PopularComboEstadosDestino();
        }

        private void ResizeColumns()
        {
            double mediaWidth = dataGridViewPedidos.Width / dataGridViewPedidos.Columns.GetColumnCount(DataGridViewElementStates.Visible);

            for (int i = dataGridViewPedidos.Columns.Count - 1; i >= 0; i--)
            {
                var coluna = dataGridViewPedidos.Columns[i];
                coluna.Width = Convert.ToInt32(mediaWidth);
            }
        }

        private object GetTablePedidos()
        {
            DataTable table = new DataTable("pedidos");
            table.Columns.Add(new DataColumn("Nome do produto", typeof(string)));
            table.Columns.Add(new DataColumn("Codigo do produto", typeof(string)));
            table.Columns.Add(new DataColumn("Valor", typeof(decimal)));
            table.Columns.Add(new DataColumn("Brinde", typeof(bool)));

            return table;
        }

        private void buttonGerarNotaFiscal_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace( txtNomeCliente.Text))
            {
                MessageBox.Show("O campo NOME DO CLIENTE é obrigatório.");
                return;
            }
            
            Pedido pedido = new Pedido
            {
                EstadoOrigem = (cboEstadoOrigem.SelectedItem as OptionItem).Value, // txtEstadoOrigem.Text,
                EstadoDestino = (cboEstadoDestino.SelectedItem as OptionItem).Value, // txtEstadoDestino.Text,
                NomeCliente = txtNomeCliente.Text
            };

            DataTable table = (DataTable)dataGridViewPedidos.DataSource;

            foreach (DataRow row in table.Rows)
            {   
                var pedidoItem = new PedidoItem()
                {
                    Brinde = Convert.ToBoolean(row.IsNull("Brinde") ? false : row["Brinde"]),
                    CodigoProduto = row["Codigo do produto"].ToString(),
                    NomeProduto = row["Nome do produto"].ToString(),
                    ValorItemPedido = Convert.ToDecimal((row.IsNull("Valor") ? 1 : row["Valor"]).ToString())
                };

                if (string.IsNullOrWhiteSpace(pedidoItem.NomeProduto))
                {
                    MessageBox.Show("O campo NOME DO PRODUTO é obrigatório.");
                    return;
                }

                if (string.IsNullOrWhiteSpace(pedidoItem.CodigoProduto))
                {
                    MessageBox.Show("O campo CÓDIGO DO PRODUTO é obrigatório.");
                    return;
                }

                pedido.ItensDoPedido.Add(pedidoItem);
            }

            _notaFiscalService.GerarNotaFiscal(pedido);            
            MessageBox.Show("Operação efetuada com sucesso.");

            // Reinicia/reseta os campos
            ResetarCampos();
        }

        private void ResetarCampos()
        {
            txtNomeCliente.Text = string.Empty;
            cboEstadoOrigem.SelectedIndex = 0;
            cboEstadoDestino.SelectedIndex = 0;
            dataGridViewPedidos.DataSource = GetTablePedidos();
            ResizeColumns();
        }

        private void PopularComboEstadosOrigem()
        {
            var estados = new[]
            {
                new OptionItem { Value = "SP", Text = "São Paulo (SP)" },
                new OptionItem { Value = "MG", Text = "Minas Gerais (MG)" }
            };

            cboEstadoOrigem.Items.AddRange(estados);
            cboEstadoOrigem.ValueMember = "Value";
            cboEstadoOrigem.DisplayMember = "Text";
            cboEstadoOrigem.SelectedIndex = 0;
        }

        private void PopularComboEstadosDestino()
        {
            var estados = new[]
            {
                new OptionItem { Value = "RJ", Text = "Rio de Janeiro (RJ)" },
                new OptionItem { Value = "MG", Text = "Minas Gerais (MG)" },
                new OptionItem { Value = "PE", Text = "Pernambuco (PE)" },
                new OptionItem { Value = "PB", Text = "Paraíba (PB)" },
                new OptionItem { Value = "PR", Text = "Paraná (PR)" },
                new OptionItem { Value = "PI", Text = "Piauí (PI)" },
                new OptionItem { Value = "RO", Text = "Rondônia (RO)" },
                new OptionItem { Value = "SE", Text = "Sergipe (SE)" },
                new OptionItem { Value = "TO", Text = "Tocantins (TO)" },
                new OptionItem { Value = "PA", Text = "Pará (PA)" }
            };

            cboEstadoDestino.Items.AddRange(estados);
            cboEstadoDestino.ValueMember = "Value";
            cboEstadoDestino.DisplayMember = "Text";
            cboEstadoDestino.SelectedIndex = 0;
        }

        private class OptionItem
        {
            public string Value { get; set; }
            public string Text { get; set; }
        }
    }
}
