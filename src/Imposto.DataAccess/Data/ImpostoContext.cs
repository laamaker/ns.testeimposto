﻿using Imposto.DataAccess.Data.Configurations;
using Imposto.Domain.Models;
using System.Data.Entity;

namespace Imposto.DataAccess.Data
{
    public class ImpostoContext : DbContext
    {
        public ImpostoContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<NotaFiscal> NotaFiscais { get; set; }
        public DbSet<NotaFiscalItem> NotaFiscalItems { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Configuração para manter os nomes das tabelas no singular
            modelBuilder.Configurations.Add(new NotaFiscalConfiguration());
            modelBuilder.Configurations.Add(new NotaFiscalItemConfiguration());
        }
    }
}
