﻿using Imposto.Domain.Models;
using System.Data.Entity.ModelConfiguration;

namespace Imposto.DataAccess.Data.Configurations
{
    public class NotaFiscalItemConfiguration : EntityTypeConfiguration<NotaFiscalItem>
    {
        public NotaFiscalItemConfiguration()
        {
            ToTable("NotaFiscalItem");
            HasKey(x => x.Id);
            Property(x => x.IdNotaFiscal).IsOptional();
            Property(x => x.Cfop).HasColumnType("varchar").HasMaxLength(5).IsOptional();
            Property(x => x.TipoIcms).HasColumnType("varchar").HasMaxLength(20).IsOptional();
            Property(x => x.BaseIcms).HasPrecision(18, 5).IsOptional();
            Property(x => x.AliquotaIcms).HasPrecision(18, 5).IsOptional();
            Property(x => x.ValorIcms).HasPrecision(18, 5).IsOptional();
            Property(x => x.NomeProduto).HasColumnType("varchar").HasMaxLength(50).IsOptional();
            Property(x => x.CodigoProduto).HasColumnType("varchar").HasMaxLength(20).IsOptional();

            Property(x => x.BaseIpi).HasPrecision(18, 5).IsOptional();
            Property(x => x.AliquotaIpi).HasPrecision(18, 5).IsOptional();
            Property(x => x.ValorIpi).HasPrecision(18, 5).IsOptional();
            Property(x => x.Desconto).HasPrecision(18, 5).IsOptional();
        }
    }
}
