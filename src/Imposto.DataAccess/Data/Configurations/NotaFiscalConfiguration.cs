﻿using Imposto.Domain.Models;
using System.Data.Entity.ModelConfiguration;

namespace Imposto.DataAccess.Data.Configurations
{
    public class NotaFiscalConfiguration : EntityTypeConfiguration<NotaFiscal>
    {
        public NotaFiscalConfiguration()
        {
            ToTable("NotaFiscal");
            HasKey(x => x.Id);
            Property(x => x.NumeroNotaFiscal).IsOptional();
            Property(x => x.Serie).IsOptional();
            Property(x => x.NomeCliente).HasColumnType("varchar").HasMaxLength(50).IsOptional();
            Property(x => x.EstadoDestino).HasColumnType("varchar").HasMaxLength(50).IsOptional();
            Property(x => x.EstadoOrigem).HasColumnType("varchar").HasMaxLength(50).IsOptional();
        }
    }
}
