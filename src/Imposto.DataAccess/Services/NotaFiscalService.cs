﻿using Imposto.DataAccess.Repositories;
using Imposto.Domain.Models;

namespace Imposto.DataAccess.Services
{
    public class NotaFiscalService : INotaFiscalService
    {
        private readonly INotaFiscalRepository _notaFiscalRepository;
        
        public NotaFiscalService()
        {
            // TODO: DI
            _notaFiscalRepository = new NotaFiscalRepository();
        }

        public void GerarNotaFiscal(Pedido pedido)
        {
            NotaFiscal notaFiscal = new NotaFiscal();
            notaFiscal.EmitirNotaFiscal(pedido);

            SalvarNotaFiscal(notaFiscal);
        }

        public void SalvarNotaFiscal(NotaFiscal notaFiscal)
        {
            _notaFiscalRepository.SalvarNotaFiscal(notaFiscal);
        }
    }
}
