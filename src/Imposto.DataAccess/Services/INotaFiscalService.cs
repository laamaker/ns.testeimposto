﻿using Imposto.Domain.Models;

namespace Imposto.DataAccess.Services
{
    public interface INotaFiscalService
    {
        void GerarNotaFiscal(Pedido pedido);
        void SalvarNotaFiscal(NotaFiscal notaFiscal);
    }
}
