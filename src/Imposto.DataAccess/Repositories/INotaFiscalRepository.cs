﻿using Imposto.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Imposto.DataAccess.Repositories
{
    public interface INotaFiscalRepository
    {   
        Task<List<NotaFiscal>> ObterNotasFiscais();
        Task<NotaFiscal> ObterNotaFiscalPorId(int id);
        void SalvarNotaFiscal(NotaFiscal notaFiscal);
                
        Task<List<NotaFiscalItem>> ObterNotaFiscalItens();
        Task<NotaFiscalItem> ObterNotaFiscalItemPorId(int id);
    }
}
