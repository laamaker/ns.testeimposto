﻿using Imposto.DataAccess.Data;
using Imposto.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Imposto.DataAccess.Repositories
{
    public class NotaFiscalRepository : INotaFiscalRepository
    {
        private const string ProcNotaFiscal = "P_NOTA_FISCAL";
        private const string ProcNotaFiscalItem = "P_NOTA_FISCAL_ITEM";
        private readonly ImpostoContext _context;

        public NotaFiscalRepository()
        {
            // TODO: DI
            _context = new ImpostoContext();
        }

        #region NotaFiscal
        public async Task<List<NotaFiscal>> ObterNotasFiscais()
        {
            var res = await _context.NotaFiscais.ToListAsync();
            return res;
        }

        public async Task<NotaFiscal> ObterNotaFiscalPorId(int id)
        {
            var res = await _context.NotaFiscais.FindAsync(id);
            return res;
        }

        public void SalvarNotaFiscal(NotaFiscal notaFiscal)
        {
            var idOutput = new SqlParameter
            {
                ParameterName = "pId",
                Direction = System.Data.ParameterDirection.InputOutput,
                SqlDbType = System.Data.SqlDbType.Int,
                Value = notaFiscal.Id
            };

            int rowsAffected = _context.Database.ExecuteSqlCommand($"Exec {ProcNotaFiscal} @pId OUTPUT, " +
                "   @pNumeroNotaFiscal, @pSerie, @pNomeCliente, @pEstadoDestino, @pEstadoOrigem",
                idOutput,
                new SqlParameter("pNumeroNotaFiscal", notaFiscal.NumeroNotaFiscal),
                new SqlParameter("pSerie", notaFiscal.Serie),
                new SqlParameter("pNomeCliente", notaFiscal.NomeCliente),
                new SqlParameter("pEstadoDestino", notaFiscal.EstadoDestino),
                new SqlParameter("pEstadoOrigem", notaFiscal.EstadoOrigem));

            notaFiscal.Id = Convert.ToInt32(idOutput.Value);

            if (notaFiscal.Id > 0 && notaFiscal.ItensDaNotaFiscal.Count > 0)
            {
                foreach (var notaFiscalItem in notaFiscal.ItensDaNotaFiscal)
                {
                    SalvarNotaFiscalItem(notaFiscal.Id, notaFiscalItem);
                }
            }
        }
        #endregion

        #region NotaFiscalItem
        public async Task<List<NotaFiscalItem>> ObterNotaFiscalItens()
        {
            var res = await _context.NotaFiscalItems.ToListAsync();
            return res;
        }

        public async Task<NotaFiscalItem> ObterNotaFiscalItemPorId(int id)
        {
            var res = await _context.NotaFiscalItems.FindAsync(id);
            return res;
        }

        public void SalvarNotaFiscalItem(int notaFiscalId, NotaFiscalItem notaFiscalItem)
        {
            var idOutput = new SqlParameter
            {
                ParameterName = "pId",
                Direction = System.Data.ParameterDirection.InputOutput,
                SqlDbType = System.Data.SqlDbType.Int,
                Value = notaFiscalItem.Id
            };

            int rowsAffected = _context.Database.ExecuteSqlCommand($"Exec {ProcNotaFiscalItem} @pId OUTPUT, " +
                "   @pIdNotaFiscal, @pCfop, @pTipoIcms, @pBaseIcms, @pAliquotaIcms, @pValorIcms, @pNomeProduto, @pCodigoProduto, " +
                "   @pBaseIpi, @pAliquotaIpi, @pValorIpi, @pDesconto",
                idOutput,
                new SqlParameter("pIdNotaFiscal", notaFiscalId),
                new SqlParameter("pCfop", notaFiscalItem.Cfop),
                new SqlParameter("pTipoIcms", notaFiscalItem.TipoIcms),
                new SqlParameter("pBaseIcms", notaFiscalItem.BaseIcms),
                new SqlParameter("pAliquotaIcms", notaFiscalItem.AliquotaIcms),
                new SqlParameter("pValorIcms", notaFiscalItem.ValorIcms),
                new SqlParameter("pNomeProduto", notaFiscalItem.NomeProduto),
                new SqlParameter("pCodigoProduto", notaFiscalItem.CodigoProduto),
                new SqlParameter("pBaseIpi", notaFiscalItem.BaseIpi),
                new SqlParameter("pAliquotaIpi", notaFiscalItem.AliquotaIpi),
                new SqlParameter("pValorIpi", notaFiscalItem.ValorIpi),
                new SqlParameter("pDesconto", notaFiscalItem.Desconto));
        }
    }
    #endregion
}
