namespace Imposto.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initialcreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.NotaFiscal",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    NumeroNotaFiscal = c.Int(nullable: false),
                    Serie = c.Int(nullable: false),
                    NomeCliente = c.String(),
                    EstadoDestino = c.String(),
                    EstadoOrigem = c.String(),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.NotaFiscalItem",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    IdNotaFiscal = c.Int(nullable: false),
                    Cfop = c.String(),
                    TipoIcms = c.String(),
                    BaseIcms = c.Double(nullable: false),
                    AliquotaIcms = c.Double(nullable: false),
                    ValorIcms = c.Double(nullable: false),
                    NomeProduto = c.String(),
                    CodigoProduto = c.String(),
                    NotaFiscal_Id = c.Int(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NotaFiscal", t => t.NotaFiscal_Id)
                .Index(t => t.NotaFiscal_Id);

        }
        
        public override void Down()
        {
            DropForeignKey("dbo.NotaFiscalItem", "NotaFiscal_Id", "dbo.NotaFiscal");
            DropIndex("dbo.NotaFiscalItem", new[] { "NotaFiscal_Id" });
            DropTable("dbo.NotaFiscalItem");
            DropTable("dbo.NotaFiscal");
        }
    }
}
