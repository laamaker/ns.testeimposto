namespace Imposto.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NotaFiscalItem_Campo_Desconto : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.NotaFiscalItem", "Desconto", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.NotaFiscalItem", "Desconto");
        }
    }
}
