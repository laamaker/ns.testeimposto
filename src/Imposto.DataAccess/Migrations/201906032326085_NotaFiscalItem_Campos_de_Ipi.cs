namespace Imposto.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NotaFiscalItem_Campos_de_Ipi : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.NotaFiscalItem", "BaseIpi", c => c.Double(nullable: false));
            AddColumn("dbo.NotaFiscalItem", "AliquotaIpi", c => c.Double(nullable: false));
            AddColumn("dbo.NotaFiscalItem", "ValorIpi", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.NotaFiscalItem", "ValorIpi");
            DropColumn("dbo.NotaFiscalItem", "AliquotaIpi");
            DropColumn("dbo.NotaFiscalItem", "BaseIpi");
        }
    }
}
