namespace Imposto.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Adicionado_Configurations_e_Campos_Decimal : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.NotaFiscal", "NumeroNotaFiscal", c => c.Int());
            AlterColumn("dbo.NotaFiscal", "Serie", c => c.Int());
            AlterColumn("dbo.NotaFiscal", "NomeCliente", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.NotaFiscal", "EstadoDestino", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.NotaFiscal", "EstadoOrigem", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.NotaFiscalItem", "IdNotaFiscal", c => c.Int());
            AlterColumn("dbo.NotaFiscalItem", "Cfop", c => c.String(maxLength: 5, unicode: false));
            AlterColumn("dbo.NotaFiscalItem", "TipoIcms", c => c.String(maxLength: 20, unicode: false));
            AlterColumn("dbo.NotaFiscalItem", "BaseIcms", c => c.Decimal(precision: 18, scale: 5));
            AlterColumn("dbo.NotaFiscalItem", "AliquotaIcms", c => c.Decimal(precision: 18, scale: 5));
            AlterColumn("dbo.NotaFiscalItem", "ValorIcms", c => c.Decimal(precision: 18, scale: 5));
            AlterColumn("dbo.NotaFiscalItem", "NomeProduto", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.NotaFiscalItem", "CodigoProduto", c => c.String(maxLength: 20, unicode: false));
            AlterColumn("dbo.NotaFiscalItem", "BaseIpi", c => c.Decimal(precision: 18, scale: 5));
            AlterColumn("dbo.NotaFiscalItem", "AliquotaIpi", c => c.Decimal(precision: 18, scale: 5));
            AlterColumn("dbo.NotaFiscalItem", "ValorIpi", c => c.Decimal(precision: 18, scale: 5));
            AlterColumn("dbo.NotaFiscalItem", "Desconto", c => c.Decimal(precision: 18, scale: 5));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.NotaFiscalItem", "Desconto", c => c.Double(nullable: false));
            AlterColumn("dbo.NotaFiscalItem", "ValorIpi", c => c.Double(nullable: false));
            AlterColumn("dbo.NotaFiscalItem", "AliquotaIpi", c => c.Double(nullable: false));
            AlterColumn("dbo.NotaFiscalItem", "BaseIpi", c => c.Double(nullable: false));
            AlterColumn("dbo.NotaFiscalItem", "CodigoProduto", c => c.String());
            AlterColumn("dbo.NotaFiscalItem", "NomeProduto", c => c.String());
            AlterColumn("dbo.NotaFiscalItem", "ValorIcms", c => c.Double(nullable: false));
            AlterColumn("dbo.NotaFiscalItem", "AliquotaIcms", c => c.Double(nullable: false));
            AlterColumn("dbo.NotaFiscalItem", "BaseIcms", c => c.Double(nullable: false));
            AlterColumn("dbo.NotaFiscalItem", "TipoIcms", c => c.String());
            AlterColumn("dbo.NotaFiscalItem", "Cfop", c => c.String());
            AlterColumn("dbo.NotaFiscalItem", "IdNotaFiscal", c => c.Int(nullable: false));
            AlterColumn("dbo.NotaFiscal", "EstadoOrigem", c => c.String());
            AlterColumn("dbo.NotaFiscal", "EstadoDestino", c => c.String());
            AlterColumn("dbo.NotaFiscal", "NomeCliente", c => c.String());
            AlterColumn("dbo.NotaFiscal", "Serie", c => c.Int(nullable: false));
            AlterColumn("dbo.NotaFiscal", "NumeroNotaFiscal", c => c.Int(nullable: false));
        }
    }
}
