﻿namespace Imposto.Domain.Enums
{
    public enum Regiao
    {
        Norte,
        Nordeste,
        CentroOeste,
        Sudeste,
        Sul
    }
}
