﻿namespace Imposto.Domain.Models
{
    public class PedidoItem
    {
        public string NomeProduto { get; set; }
        public string CodigoProduto { get; set; }        
        public decimal ValorItemPedido { get; set; }
        public bool Brinde { get; set; }        
    }
}
