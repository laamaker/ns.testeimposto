﻿using Imposto.Domain.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Imposto.Domain.Models
{
    public class Estado
    {
        public string Nome { get; set; }

        public string Sigla { get; set; }

        public Regiao Regiao { get; set; }

        private static List<Estado> Estados = new List<Estado>
        {
            new Estado { Nome = "São Paulo", Sigla = "SP", Regiao = Regiao.Sudeste },
            new Estado { Nome = "Rio de Janeiro", Sigla = "RJ", Regiao = Regiao.Sudeste },
            new Estado { Nome = "Minas Gerais", Sigla = "MG", Regiao = Regiao.Sudeste },

            new Estado { Nome = "Pernambuco", Sigla = "PE", Regiao = Regiao.Nordeste },
            new Estado { Nome = "Paraíba", Sigla = "PB", Regiao = Regiao.Nordeste },
            new Estado { Nome = "Piauí", Sigla = "PI", Regiao = Regiao.Nordeste },
            new Estado { Nome = "Sergipe", Sigla = "SE", Regiao = Regiao.Nordeste },

            new Estado { Nome = "Tocantins", Sigla = "TO", Regiao = Regiao.Norte },
            new Estado { Nome = "Pará", Sigla = "PA", Regiao = Regiao.Norte },
            new Estado { Nome = "Rondônia", Sigla = "RO", Regiao = Regiao.Norte },

            new Estado { Nome = "Paraná", Sigla = "PR", Regiao = Regiao.Sul }
        };

        public static List<Estado> GetTodosEstados()
        {
            return Estados.ToList();
        }

        public static List<Estado> GetEstadosPorRegiao(Regiao regiao)
        {
            return Estados.Where(x => x.Regiao == regiao).ToList();
        }

        public static Estado GetEstadoPorSigla(string sigla)
        {
            return Estados.FirstOrDefault(x => x.Sigla == sigla);
        }
    }
}
