﻿using System;
using Imposto.DataAccess.Repositories;
using Imposto.Domain.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Threading.Tasks;

namespace Imposto.Test.DataAccess
{
    [TestClass]
    public class NotaFiscalRepositoryTest
    {
        private INotaFiscalRepository _notaFiscalRepository;

        [TestInitialize]
        public void Initialize()
        {
            var notaFiscalRepository = new Mock<INotaFiscalRepository>();
            notaFiscalRepository.Setup(x => x.SalvarNotaFiscal(It.Is<NotaFiscal>(y => y == null))).Throws<ArgumentException>();

            _notaFiscalRepository = notaFiscalRepository.Object;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Should_ThrowException_If_NotaFiscal_IsNull()
        {
            _notaFiscalRepository.SalvarNotaFiscal(null);
        }
    }
}
