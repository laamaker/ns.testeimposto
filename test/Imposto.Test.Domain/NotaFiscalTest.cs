﻿using System.Collections.Generic;
using System.Linq;
using Imposto.Domain.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Imposto.Test.Domain
{
    [TestClass]
    public class NotaFiscalTest
    {
        [TestMethod]
        public void Should_Set_Desconto_With_10_Percent()
        {
            var pedido = new Pedido
            {
                EstadoDestino = "SP",
                ItensDoPedido = new List<PedidoItem> { new PedidoItem() }
            };
                        
            var notaFiscal = new NotaFiscal();
            notaFiscal.ProcessarNotaFiscal(pedido);
            
            Assert.AreEqual(0.10M, notaFiscal.ItensDaNotaFiscal.First().Desconto);
        }

        [TestMethod]
        public void Should_Set_AliquotaIpi_To_Zero_If_Brinde()
        {
            var pedido = new Pedido
            {
                ItensDoPedido = new List<PedidoItem> { new PedidoItem { Brinde = true } }
            };
                        
            var notaFiscal = new NotaFiscal();
            notaFiscal.ProcessarNotaFiscal(pedido);
            
            Assert.IsTrue(notaFiscal.ItensDaNotaFiscal.First().AliquotaIpi == 0);
        }
    }
}
